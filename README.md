# OpenML dataset: mne-sample-meg-auditory

https://www.openml.org/d/43884

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

MEG data from auditory stimulation experiment using 305 sensors.
The design matrix/forward operator is `data[:, :7498]`.
The measurements for left stimulation are `data[:, 7498:7583]`.
The measurements for right stimulation are `data[:, 7583:]`.

The data was generated with the following script:
```
import mne
import numpy as np
from mne.datasets import sample
from mne.inverse_sparse.mxne_inverse import _prepare_gain
# this script used MNE 1.0.2

condition = "Right Auditory"
data_path = sample.data_path() + '/MEG/sample'
loose = 0
depth = 0.8

fwd_fname = data_path + '/sample_audvis-meg-eeg-oct-6-fwd.fif'
ave_fname = data_path + '/sample_audvis-ave.fif'
cov_fname = data_path + '/sample_audvis-shrunk-cov.fif'

# Read noise covariance matrix
noise_cov = mne.read_cov(cov_fname)
# Handling forward solution
forward = mne.read_forward_solution(fwd_fname)
targets = {}

for condition in ["Left Auditory", "Right Auditory"]:
    evoked = mne.read_evokeds(
        ave_fname, condition=condition, baseline=(None, 0))
    evoked.crop(tmin=0.04, tmax=0.18)
    evoked = evoked.pick_types(eeg=False, meg=True)

    # Handle depth weighting and whitening (here is no weights)
    forward, gain, gain_info, whitener, _, _ = _prepare_gain(
        forward, evoked.info, noise_cov, pca=False, depth=depth,
        loose=loose, weights=None, weights_min=None, rank=None)

    # Select channels of interest
    sel = [evoked.ch_names.index(name) for name in gain_info['ch_names']]
    M = evoked.data[sel]

    # Whiten data
    M = whitener @ M
    targets[condition] = M

# gain is independent of condition:
data = np.hstack([gain, targets["Left Auditory"], targets["Right Auditory"]])
```

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43884) of an [OpenML dataset](https://www.openml.org/d/43884). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43884/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43884/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43884/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

